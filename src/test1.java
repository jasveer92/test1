import java.util.Scanner;

public class test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//1. ask the user for input
		// how many software do you want?
		Scanner keyboard = new Scanner(System.in);
		System.out.println("how many software do you want?");
		int numSoftware= keyboard.nextInt();
		double discount =0;
		//2 calculate the discount
		if (numSoftware>=10 && numSoftware<=19){
			//discount=20%
			discount=0.20;
		}
		else if (numSoftware>=20 && numSoftware<=49){
			//discount=30%
			discount=0.30;
		}
		else if (numSoftware>=50 && numSoftware<=99){
			//discount=40%
			discount=0.40;
		}
		else if (numSoftware>=100){
			//discount=50%
			discount=0.50;
		}
		double subtotal= 99 * numSoftware;
		double discountAmount=subtotal * discount;
		double finalAmount= subtotal-discountAmount;
		//3 show output
		System.out.println("Subtotal: $" +subtotal);
		System.out.println("Discount: $" +discountAmount);
		System.out.println("Final amount: $" +finalAmount);
		
	}

}
